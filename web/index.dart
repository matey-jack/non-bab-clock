import 'dart:html';
import 'dart:math' hide E;
import 'dart:svg' hide Point;

Point a2c(Point origin, num r, num phi) {
  return new Point(origin.x + r * sin(phi), origin.y - r * cos(phi));
}

final center = const Point<int>(50, 50);

Iterable<Point> nonBabRimPoints() {
  const N = 5;
  const radius = 45;
  const turn = 0.5;
  const A = 0.95;
  const B = 0.92;
  const E = 0.03;
  final points = new List<Point>();
  for (int i = 0; i <= N; i++) {
    points.add(a2c(center, radius * A, (turn + i - 0.5 - E) * 2 * PI / N));
    points.add(a2c(center, radius * B, (turn + i - 0.5) * 2 * PI / N));
    points.add(a2c(center, radius * A, (turn + i - 0.5 + E) * 2 * PI / N));
    points.add(a2c(center, radius, (turn + i) * 2 * PI / N));
  }
  points.removeLast();
  points.removeLast();
  return points;
}

String pointsToString(Iterable<Point> points) {
  return points.map((p) => "${p.x},${p.y}").join(' ');
}

SvgElement line(Point a, Point b) {
  const fractionDigits = 4;
  final result = new LineElement();
  result.attributes = {
    "x1": a.x.toStringAsFixed(fractionDigits),
    "y1": a.y.toStringAsFixed(fractionDigits),
    "x2": b.x.toStringAsFixed(fractionDigits),
    "y2": b.y.toStringAsFixed(fractionDigits),
  };
  return result;
}

SvgElement halfNonBabHand(num inR, num outR, num phi, num deltaPhi) {
  final nonBabHand =
      line(a2c(center, inR, phi + deltaPhi), a2c(center, outR, phi));
  nonBabHand.attributes["class"] = "bo-hand";
  return nonBabHand;
}

void main() {
  // querySelector('#messages').text = 'Your Dart app is running.';

  final SvgElement nonBabSvg = querySelector('#non-bab-clock-svg');
  final SvgElement romanSvg = querySelector('#roman-clock-svg');

  final romanRim = new CircleElement();
  romanRim.attributes = {
    "cx": (romanSvg.clientWidth / 2).toString(),
    "cy": (romanSvg.clientHeight / 2).toString(),
    "r": (min(romanSvg.clientHeight, romanSvg.clientWidth) / 2.2).toString(),
    "class": "roman-rim",
  };
  // romanSvg.children.add(romanRim);

  final nonBabRim = new PolygonElement();
  nonBabRim.attributes = {
    "points": pointsToString(nonBabRimPoints()),
    "class": "non-bab-rim",
  };
  // nonBabSvg.children.add(nonBabRim);

  final dt = new DateTime.now();
  final dur = dt.difference(new DateTime(dt.year, dt.month, dt.day));
  final phi = 2 * PI * (dur.inSeconds / 3600 - 6) / 24 * 30 / 10;
  nonBabSvg.children.add(halfNonBabHand(20, 32, phi, 2 * PI / 30));
  nonBabSvg.children.add(halfNonBabHand(20, 32, phi, -2 * PI / 30));

  final miHand = line(a2c(center, 7, phi*30), a2c(center, 32, phi*30));
  miHand.attributes["class"] = "mi-hand";
  nonBabSvg.children.add(miHand);


  // final baseHand = line(a2c(center, 10, 0), a2c(center, 32, 0));
  // baseHand.attributes["class"] = "roman-hand";
  // romanSvg.children.add(baseHand);

  final romanPhi = (dt.hour + dt.minute / 60) / 24 * 2 * PI;
  final romanHand = line(a2c(center, 0, romanPhi), a2c(center, 30, romanPhi));
  romanHand.attributes["class"] = "roman-hand";
  // romanSvg.children.add(romanHand);

  final roMinPhi = (dt.minute + dt.second / 60) / 60 * 2 * PI;
  final roMinHand = line(a2c(center, 0, roMinPhi), a2c(center, 40, roMinPhi));
  roMinHand.attributes["class"] = "ro-min-hand";
  // romanSvg.children.add(roMinHand);

}
