import 'dart:html';
import 'dart:math';

  
void drawPolygon(CanvasRenderingContext2D ctx, int n, num r, num x, num y) {
  ctx.beginPath();
  ctx.moveTo(x, y + 1*r);
  for (int i = 1; i <= n; i++) {
    double alpha = i*PI*2/n;
    double cx = x + sin(alpha)*r;
    double cy = y + cos(alpha)*r;
	  ctx.lineTo(cx, cy);
  }
  ctx.stroke();
}

void main() {
  final CanvasElement canvas = querySelector('#playground');
  final CanvasRenderingContext2D context = canvas.getContext('2d');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  double radius = 30.0;
  double delta_x = 2 * radius * cos(PI/6);
  double delta_y = radius + radius * sin(PI/6);
  for (int jy = 0; delta_y*jy <= canvas.height; jy++) {
    double offset = (jy % 2) / 2;
    for (int ix = 0; delta_x*ix <= canvas.width; ix++) {
      drawPolygon(context, 6, radius, delta_x*(ix+offset), delta_y*(jy+0.5));
    }
  }

}
